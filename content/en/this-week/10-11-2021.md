---
title: 'Week of 10/11/2021'
description: Catch up with the latest updates from Grey Software!
category: 'Weekly Updates'
position: 3001
---

## Overview

This week we published a condensed and revised iteration of our [`/pitch`](https://grey.software/pitch), and continued
to make gradual updates throughout our websites and education initiatives.

<cta-button text="v4.2 Milestone" link="https://gitlab.com/groups/grey-software/-/milestones/2"></cta-button>
<cta-button text="Pitch" link="https://grey.software/pitch"></cta-button>

> **Reminder of our major goals from last week**
>
> 1. Refine and update /pitch and Pioneer project description
> 2. Write emails to Social Capital, Underscore VC, OSS Capital, and others
> 3. Reach out and strategize about uni workshops at PAK-UETM, PAK-LUMS, CAN-UTM

### What we did this past week

1. Published a new iteration of [`/pitch`](https://grey.software/pitch)

![Pitch Slide](https://grey.software/pitch/10.png)

2. Updated our Pioneer Project Description

3. We sent a letter to Social Capital. Other firms and investors to follow!

4. We began outlining our workshops & continued to fill gaps in our documentation!

5. We experimented with getting a newsletter up and running using [the Ghost platform](https://ghost.org)

<cta-button link="https://grey-software.ghost.io" text="Newletter Experiment"></cta-button>

6. We started compiling our yearly financials spreadsheet

### Our goals for Next Week

1. Refine /pitch and start campaigning more publicly for Founding Partners

2. Continue outlining workshops and filling gaps in our documentation!

3. Publish our yearly financials on this org website!

## Team Updates

### Faraz

This week I worked on the [automation dashboard's](https://gitlab.com/groups/grey-software/-/epics/5) issue
[#5](https://gitlab.com/grey-software/automation/-/issues/5) to implement a generic method that will set the foundation
for a lot of important automation tasks that we need to achieve in the future.

This method opens a form on the dashboard with options to select the target repository, remote file path to be updated,
and commit message. On confirming, the target repository's file gets updated and a new merge request is created.

### Laiba

This week I collaborated on revising and refining the
[resources' contribution](https://gitlab.com/grey-software/resources/-/issues/13) and
[gitlab management](https://gitlab.com/grey-software/resources/-/issues/14) documents alongside creating process
flowcharts that fit more with Grey Software branding.
